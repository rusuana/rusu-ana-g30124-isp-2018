package g30124.rusu.ana.l7.e3;

import java.util.TreeSet;

public class  BankTreeSet extends BankAccount{
	
	static TreeSet<BankAccount> account = new TreeSet<>();
	
	public BankTreeSet(String owner, double balance) {
		super(owner, balance);
	
	}

	public void addAccount(String owner, double balance) {
		BankTreeSet b= new BankTreeSet(owner,balance);
		account.add(b);
	}
	
	public void printAccounts() {
		System.out.println("TreeSet sorted by balance: ");
		for(BankAccount i:account)
			System.out.println(i.getBalance()+" "+i.getOwner());
	}
	
	public void printAccounts(int minBalance, int maxBalance) {
		System.out.println("Accounts between "+minBalance+" and "+maxBalance+":");
		for(BankAccount i:account) {
			if(i.getBalance()>minBalance && i.getBalance()<maxBalance)
				System.out.println("Account name: "+i.getOwner()+", the value: "+i.getBalance());
		}
	}
	
	public BankAccount getAccount(String owner) {
		for(BankAccount i:account) {
			if(i.getOwner().equals(owner))
				System.out.println(i.getBalance()+" "+i.getOwner());
		}
		return null;
	}
	
	public static void main(String[] args) {

		BankTreeSet b = new BankTreeSet("Popescu",2500);
		b.addAccount("Ioan",1500);
		b.addAccount("Paul", 1250);
		b.addAccount("Daniela", 1600);
		b.addAccount("Diana", 2000);
		b.printAccounts();
		System.out.println("\n\n");
		b.printAccounts(1400, 2000);
	}
}