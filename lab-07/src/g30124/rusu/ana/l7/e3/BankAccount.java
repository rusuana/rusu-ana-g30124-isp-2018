package g30124.rusu.ana.l7.e3;


public class BankAccount implements Comparable<BankAccount>{
	
	protected String owner;
	protected double balance;
	
	public BankAccount(String owner, double balance) {
		this.owner = owner;
		this.balance = balance;
	}
	
	public void withdraw(double amount) {
		balance-=amount;
	}
	
	public void deposit(double amount) {
		balance+=amount;
	}
	
	public String getOwner() {
		return owner;
	}
	
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	public double getBalance() {
		return balance;
	}
	
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	public int compareTo(BankAccount b) {
		return (int) (balance-b.getBalance());
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof BankAccount){
			if(this.owner.equals(((BankAccount) obj).getOwner()) && this.balance == ((BankAccount)obj).getBalance())
	               return true;
		}
		return false;
	}
	
	@Override 
	public int hashCode(){
		return (int) (balance+ owner.hashCode());
		
	}
	
	public static void main(String[] args) {
		BankAccount account1=new BankAccount("Liviu",100);
		BankAccount account2=new BankAccount("Liviu",100);
		
		
		System.out.println(account1.equals(account2));
		account2.withdraw(100);
		System.out.println(account2.equals(account1));
	
	}
}
