package g30124.rusu.ana.l7.e1;

public class BankAccount {
	private String owner;
	private double balance;
	
	public BankAccount(String owner, double balance) {
		this.owner = owner;
		this.balance = balance;
	}
	
	public void withdraw(double amount) {
		balance-=amount;
	}
	
	public void deposit(double amount) {
		balance+=amount;
	}
	
	public String getOwner() {
		return owner;
	}
	
	public void setOwner(String owner) {
		this.owner = owner;
	}
	
	public double getBalance() {
		return balance;
	}
	
	public void setBalance(double balance) {
		this.balance = balance;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof BankAccount){
			if(this.owner.equals(((BankAccount) obj).getOwner()) && this.balance == ((BankAccount)obj).getBalance())
	               return true;
		}
		return false;
	}
	
	@Override 
	public int hashCode(){
		return (int) (balance+ owner.hashCode());
		
	}
	
	
	public static void main(String[] args) {
		BankAccount account1=new BankAccount("Liviu",100);
		BankAccount account2=new BankAccount("Liviu",100);
		BankAccount account3=new BankAccount("Maria",100);
		

		 if(account1.equals(account2)) 
			 System.out.println("Conturile account1 si account2 sunt egale.");
		 else
			 System.out.println("Conturile account1 si account2 nu sunt egale.");
		 account2.withdraw(300);
		 if(account1.equals(account2)) 
			 System.out.println("Conturile account1 si account2 sunt egale.");
		 else
			 System.out.println("Conturile account1 si account2 nu sunt egale.");
		 if(account2.equals(account3)) 
			 System.out.println("Conturile account2 si account3 sunt egale.");
		 else
			 System.out.println("Conturile account2 si account3 nu sunt egale.");
	}
}
