package g30124.rusu.ana.l7.e4;

public class Definition {
	  
	private String description;
	
	public Definition(String d) {
		this.description = d;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String d) {
		this.description = d;
	}
}