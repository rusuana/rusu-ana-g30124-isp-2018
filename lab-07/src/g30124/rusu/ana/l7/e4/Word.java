package g30124.rusu.ana.l7.e4;

public class Word {
	
    private String word;

    public Word(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
    @Override
    public boolean equals(Object o){
        Word w = (Word) o;
        if (w.getWord().equalsIgnoreCase(((Word) o).getWord()))
            return true;
        return false;
    }

    @Override
    public int hashCode(){
        int sum = 0;
        for (int i = 0 ; i <word.length(); i++){
            sum += word.charAt(i);
        }
        return sum;
    }
}
