package g30124.rusu.ana.l7.e4;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Main {
    public static void printMenu() {
        System.out.println("1.Add word");
        System.out.println("2.Find word");
        System.out.println("3.Print all words");
        System.out.println("4.Print all definitions");
        System.out.println("5.Exit");
        System.out.println("Option:");
    }

    public static void main(String[] args) throws IOException {


        HashMap<Word, Definition> map = new HashMap<Word, Definition>();
        Dictionary dict = new Dictionary(map);
        BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
        int option;
        while (true) {
            printMenu();
            option = Integer.parseInt(r.readLine());
            switch (option) {
                case 1:
                    System.out.print("\nRead a word:");
                    String w = r.readLine();
                    System.out.print("Read word definition:");
                    String definition = r.readLine();
                    dict.addWord(new Word(w), new Definition(definition));
                    break;
                case 2:
                    System.out.print("Read w:");
                    String w2 = r.readLine();
                    try {
                        System.out.println("Definition: " + dict.getDefinition(new Word(w2)).getDescription());
                    } catch (NullPointerException e) {
                        System.out.println("No such a w!");
                    }
                    break;
                case 3:
                    dict.getAllWords();
                    break;
                case 4:
                    dict.getAllDefinition();
                    break;
                case 5:
                    System.exit(0);
                    break;
                default:
                    System.out.println("Wrong option!");
                    break;
            }
            System.out.println("Press any key...");
            r.readLine();
        }
    }
}
