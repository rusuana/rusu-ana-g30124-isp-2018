package g30124.rusu.ana.l7.e4;

import java.util.Collection;
import java.util.HashMap;

public class Dictionary {

    private HashMap<Word, Definition> map;
    Dictionary(HashMap map){
        this.map = map;
    }

    public void addWord(Word w, Definition d){
        map.put(w, d);
    }

    public Definition getDefinition(Word w){
        return (Definition)map.get(w);
    }

    public void getAllWords(){
        Collection<Word> wordSet = map.keySet();
        System.out.println("Words from dictionary are:");
        for (Word x: wordSet)
            System.out.println(x.getWord());
    }

    public void getAllDefinition(){
        Collection<Definition> defintion = map.values();
        System.out.println("Definitions from dictionary are:");
        for (Definition x: defintion)
            System.out.println(x.getDescription());
    }
}
