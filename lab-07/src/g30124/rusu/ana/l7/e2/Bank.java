package g30124.rusu.ana.l7.e2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Bank extends BankAccount{
	
	static ArrayList<BankAccount> account = new ArrayList<>();
	
	public Bank(String owner, double balance) {
		super(owner, balance);
	
	}

	public void addAccount(String owner, double balance) {
		Bank b= new Bank(owner,balance);
		account.add(b);
	}
	
	public void printAccounts() {
		Collections.sort( account);
		System.out.println("List sorted by balance: ");
		for(BankAccount i:account)
			System.out.println(i.getBalance()+" "+i.getOwner());
	}
	public void printAccounts(int minBalance, int maxBalance) {
		System.out.println("Accounts between "+minBalance+" and "+maxBalance+":");
		for(BankAccount i:account) {
			if(i.getBalance()>minBalance && i.getBalance()<maxBalance)
				System.out.println("Account name: "+i.getOwner()+", the value: "+i.getBalance());
		}
	}
	
	public BankAccount getAccount(String owner) {
		for(int i=0; i<account.size(); i++) {
			if(account.get(i).getOwner().equals(owner))
				System.out.println(account.get(i).getBalance()+" "+account.get(i).getOwner());
		}
		return null;
	}
	
	public static void main(String[] args) {

		Bank b = new Bank("Popescu",2500);
		b.addAccount("Ioan",1500);
		b.addAccount("Paul", 1250);
		b.addAccount("Daniela", 1600);
		b.addAccount("Diana", 2000);
		b.printAccounts();
		System.out.println("\n\n");
		b.printAccounts(1400, 2000);
		System.out.println("\n\n");
		
		
		Collections.sort(account, new Comparator<BankAccount>() {
		    public int compare(BankAccount one, BankAccount other) {
		        return one.getOwner().compareTo(other.getOwner());
		    }
		   });
		System.out.println("Accounts sorted by owner:");
		for(BankAccount i:account) {
			System.out.println(i.getOwner()+" "+i.getBalance());
		}
	}

}