package g30124.rusu.ana.l5.e1;


public abstract class Shape{

	protected String color;
	protected boolean filled;
	public abstract double getPerimeter();
	public abstract double getArea();
	
	public Shape(){
		this.color = "yellow";
		this.filled = true;
	}
	
	public Shape(String color, boolean filled){
		this.color = color;
		this.filled = filled;
	}
	
	public boolean isFilled(){
		return this.filled;
	}
	
	public void setFilled(boolean filled){
		this.filled = filled;
	}
	
	public String getColor(){
		return this.color;
	}
	
	public void setColor(String color){
		this.color = color;
	}
	
	 @Override
	   public String toString() {
	       return "Shape{" + "color='" + color + '\'' + ", filled=" + filled + '}';
	   }
}
