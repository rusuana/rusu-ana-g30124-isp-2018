package g30124.rusu.ana.l5.e1;

public class Circle extends Shape{

		protected double radius;
		
		public Circle(){
			this.radius = 5.0;
		}
		
		public Circle(double radius){
			this.radius = radius;
		}
		
		public Circle(double radius, String color, boolean filled){
			super(color,filled);
			this.radius = radius;
		}
		
		public double getRadius(){
			return this.radius;
		}
		
		public void setRadius(double radius){
			this.radius = radius;
		}
		
		public double getArea(){
			return Math.PI*this.radius*this.radius;
		}
		
		public double getPerimeter(){
			return 2*Math.PI*this.radius;
		}
		
		@Override
		public String toString(){
		      return "Circle {" + "color= '" + color + '\'' + ", filled= " + filled + ", radius= " + radius + '}';
		}
		
		public static void main(String[] args){
			
			Circle c = new Circle(5,"blue",true);
			System.out.println("Radius of circle is: " +c.getRadius());
			System.out.println("Perimeter of circle is: " +c.getPerimeter());
			System.out.println("Area of circle is: " +c.getArea());
			System.out.println(c.toString());
		}
}