package g30124.rusu.ana.l5.e1;

public class Square extends Rectangle{

	public Square(){
	}
	
	public Square(double side){
		super(side,side);
	}
	public Square(double side, String color, boolean filled){	
		super(side,side,color,filled);
	}
	
	public double getSide(){
		return super.getLength();
		}
	
	public void setSide(double side){
		super.setLength(side);
        super.setWidth(side);
	}
	
	@Override
	public void setWidth(double side){
		super.width = side;
	}
	
	@Override
	public void setLength(double side){
		super.length =side;
	}
	
	@Override
	public String toString(){
		return "Square{" + "color='" + color + '\'' + "width=lenght= " + width + ", filled=" + filled + ", side=" + getSide() + '}';
}
	
	
	public static void main(String[] args){
		Square s = new Square(3.0,"green",true);
		s.setSide(3.5);
		System.out.println("Side of square is: " +s.getSide());
		System.out.println("Perimeter of square is: " +s.getPerimeter());
		System.out.println("Area of square is: " +s.getArea());
		System.out.println(s.toString());
	}
}

