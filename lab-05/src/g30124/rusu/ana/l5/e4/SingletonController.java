package g30124.rusu.ana.l5.e4;

import java.util.concurrent.TimeUnit;
import g30124.rusu.ana.l5.e3.LightSensor;
import g30124.rusu.ana.l5.e3.TemperatureSensor;

public class SingletonController{

    private static volatile SingletonController instance = null;
    LightSensor lightSensor = new LightSensor();
    TemperatureSensor tempSensor = new TemperatureSensor();
    
    public SingletonController(){
    }

    public static SingletonController getInstance(){
        synchronized (SingletonController.class){
            if (instance == null){
                instance = new SingletonController();
            }
        }
        return instance;
    }
    
    public void control(){
		for(int i=0; i<20; i++){
			System.out.println("Light sensor value: "+ lightSensor.readValue());
        	System.out.println("Temperature sensor value " + tempSensor.readValue());
        	try{
        		TimeUnit.SECONDS.sleep(1);
        		}
        	catch (InterruptedException e){
        		System.err.println(e);
        	}
		}
    }

    public static void main(String[] args){
        SingletonController singletonController = new SingletonController();
        singletonController.control();
    }
}
