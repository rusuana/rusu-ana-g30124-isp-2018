package g30124.rusu.ana.l5.e2;

public class ProxyImage implements Image{
	 
	   private Image image;
	   protected String fileName;
	 
	   public ProxyImage(String fileName, boolean rotated){
	      this.fileName = fileName;
	      if(rotated == true)
	    	  this.image  = new RotatedImage(fileName);
	      else
	    	  this.image = new RealImage(fileName);
	   }

	   @Override
	   public void display(){
	      image.display();
	   }
	}