package g30124.rusu.ana.l5.e3;

public abstract class Sensor{

	protected String location;
	public abstract int readValue();
	
	public String getLocation(){
		return this.location;
	}
}