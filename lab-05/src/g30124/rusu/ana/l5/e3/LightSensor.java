package g30124.rusu.ana.l5.e3;

import java.util.Random;

public class LightSensor extends Sensor{

	public int readValue(){
		Random light = new Random();
		return light.nextInt(100);
	}
}