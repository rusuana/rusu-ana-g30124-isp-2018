package g30124.rusu.ana.l5.e3;

import java.util.concurrent.TimeUnit;

public class Controller{
	
	private LightSensor lightSensor;
	private TemperatureSensor tempSensor;

	public Controller(LightSensor lightSensor, TemperatureSensor tempSensor){
		this.lightSensor = lightSensor;
		this.tempSensor = tempSensor;
	}

	public void control(){
		for(int i=0; i<20; i++){
			System.out.println("Light sensor value: "+ lightSensor.readValue());
        	System.out.println("Temperature sensor value " + tempSensor.readValue());
        	try{
        		TimeUnit.SECONDS.sleep(1);
        		}
        	catch (InterruptedException e){
        		System.err.println(e);
        	}
		}
	}

	public static void main(String[] args){
		Controller controller = new Controller(new LightSensor(), new TemperatureSensor());
		controller.control();
	}
}
