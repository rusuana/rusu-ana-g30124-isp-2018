package g30124.rusu.ana.l5.e3;
import java.util.Random;

public class TemperatureSensor extends Sensor{

	public int readValue(){
		Random temp = new Random();
		return temp.nextInt(100);
	}
	
}
