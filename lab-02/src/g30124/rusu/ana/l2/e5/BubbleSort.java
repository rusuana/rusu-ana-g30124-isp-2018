package g30124.rusu.ana.l2.e5;
import java.util.Random;

public class BubbleSort {	
	 static void bubbleSort(int[] x) {  
	        int n = x.length;  
	        int aux = 0;  
	        for(int i=0; i < n; i++){  
	        	for(int j=1; j < (n-i); j++){  
	        		if(x[j-1] > x[j]){             
	        			aux = x[j-1];
	        			x[j-1] = x[j];  
	        			x[j] = aux;  
	                }          
	        	}  
	        }  
	 }
	 public static void main(String[] args){ 
		 int[] x = new int[10];
		  Random r = new Random();	  
		  System.out.println("Before sorting the vector: " );
		  for(int i=0; i<10; i++){
			  x[i] = r.nextInt();
			  System.out.println(+x[i] );
		  } 
		  System.out.println("\nAfter sorting the vector: " );
		  bubbleSort(x);
		  for(int i=0; i<10; i++){
			  System.out.println(+x[i] );
		  }
	 }
}