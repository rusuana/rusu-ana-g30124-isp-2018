package g30124.rusu.ana.l2.e4;
import java.util.Scanner;

public class ElementMaximDinVector {
	static int max(int a,int b){
		if(a>b)
			return a;
		else return b;
	}
	public static void main(String[] args){	
		int i,n;
		int[] x = null;
		Scanner in = new Scanner(System.in);
		System.out.println("Input n = ");
		n = in.nextInt();
		x = new int[n];
		for(i=0; i<n; i++){
			System.out.println("Element "+i+" from the vector is: ");
			x[i] = in.nextInt(); 
			}
		in.close();
		int maxim = x[0];
		for(i=0; i<n; i++)
			if(max(x[i],maxim)==x[i])
				maxim=max(x[i],maxim);
		System.out.println("The maximum in the vector is: " +maxim);
	}
}