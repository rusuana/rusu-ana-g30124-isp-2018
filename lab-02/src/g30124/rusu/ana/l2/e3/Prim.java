package g30124.rusu.ana.l2.e3;
import java.util.Scanner;

public class Prim {
	static int prim(int n){
		int i,s=0;
		for(i=1; i<=n; i++)
			if(n%i==0)
				s++;
		return s;
	}
	public static void main(String[] args){
		int i,s=0;
		int a,b;
		Scanner in = new Scanner(System.in);
		System.out.println("Input a: ");
		a = in.nextInt();
		System.out.println("Input b: ");
		b = in.nextInt();
		in.close();
		System.out.println("The prime numbers in the range: ");
		for(i=a; i<=b; i++)
			if(prim(i)==2){
				System.out.printf("%d ",i);
				s++;
			}
		System.out.println("\nNumber of prime numbers: "+s);
	}
}