package g30124.rusu.ana.l2.e1;
import java.util.Scanner;

public class Maxim {
	public static void main(String [] args){   
		int a,b;
		Scanner in = new Scanner(System.in);
		System.out.println("Input a = ");
		a = in.nextInt();
		System.out.println("Input b= ");
		b = in.nextInt();
		in.close();
		if(a>b)
			System.out.println("The maximum is a= "+a);
		else
			System.out.println("The maximum is b= "+b);
	}
}
