package g30124.rusu.ana.l2.e6;
import java.util.Scanner;

public class Factorial {
	
	//Non-recursive method
	static int fact1(int n){
		int i,s=1;
		for(i=1; i<=n;i++)
			s*=i;
		return s;
	}
	
	//Recursive method
	static int fact2(int n){
		if (n==1) 
			return 1;
		else 
			return n*fact2(n-1);
	}

	public static void main(String[] args){
		int n;
		Scanner in = new Scanner(System.in);
		System.out.println("Input n = ");
		n = in.nextInt();
		in.close();
		System.out.println("Factorial of non-recursive: "+fact1(n));
		System.out.println("Factorial of recursive: "+fact2(n));
	}
}
