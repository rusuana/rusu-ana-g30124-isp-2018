package g30124.rusu.ana.l6.e4;

public class CharSeq implements CharSequence{
	
	private String s;

    public CharSeq(String s){
        this.s = s;
    }
    
    public int length(){
    	if(s!=null)
    		return s.length();
    	return 0;
    }
    
    public char charAt(int i){
    	if(s!=null)
    		return s.charAt(i);
    	return 0;
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        if(this.s != null){
            return this.s.subSequence(start,end);
        }
        return null;
    }
}
