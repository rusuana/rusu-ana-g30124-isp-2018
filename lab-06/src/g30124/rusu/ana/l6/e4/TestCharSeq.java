package g30124.rusu.ana.l6.e4;

import org.junit.Test;
import static junit.framework.Assert.assertEquals;

public class TestCharSeq {
	 
	private CharSeq sequence = new CharSeq("Ana este numele meu");

	@Test
    	public void testLength(){
			//System.out.println(sequence.length());
			assertEquals(19, sequence.length());
		}
	
	@Test
	    public void testCharAt(){
		   //System.out.println(sequence.charAt(0));
	        assertEquals('A', sequence.charAt(0));
	    }   

	@Test
	    public void testSubSequence(){
	        //System.out.println(sequence.subSequence(0,3));
	        assertEquals("Ana", (sequence.subSequence(0,3)));
	    }
}
