package g30124.rusu.ana.l6.e5;

import java.awt.*;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        DrawingBoard b1 = new DrawingBoard();

        System.out.println("Enter number of bricks: ");
        int bricks = s.nextInt();
        System.out.println("Enter high of brick: ");
        int h = s.nextInt();
        System.out.println("Enter width of brick: ");
        int w = s.nextInt();
       /* System.out.println("Enter position x: ");
        int x = s.nextInt();
        System.out.println("Enter position y: ");
        int y = s.nextInt();*/
        int x=325, y=40;
        Pyramid p = new Pyramid(15,30,x,y,Color.green);

        int i = 1; 
        while(bricks!=0){
            if(bricks-i>=0){
                for(int j=1;j<=i;j++){ 
                    b1.addPyramid(new Pyramid(h,w, p.getX()+(-i*w/2+j*w), p.getY()+(i*h), Color.green));
                }
                bricks-=i;
                i++;
            }
            if(bricks-i<0) {
            	 System.out.println("Number of unused bricks is: "+(bricks));
            	 break;
            }
            }
        }
    }
