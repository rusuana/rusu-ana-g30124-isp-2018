package g30124.rusu.ana.l6.e5;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class DrawingBoard extends JFrame {
    private ArrayList<Pyramid> p = new ArrayList<>();

    public DrawingBoard(){
        this.setTitle("Pyramid");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(700,700);
        this.setVisible(true);
    }

    public boolean addPyramid(Pyramid pyramid){
        if(!p.contains(pyramid)){
            p.add(pyramid);
            this.repaint();
            return true;
        }
        return false;
    }

    public void paint(Graphics g){ 
        for(int i=0; i<p.size(); i++){
            p.get(i).draw(g);
        }
    }
}
