package g30124.rusu.ana.l6.e5;

import java.awt.*;

public class Pyramid {
	
    private int high;
    private int width;
    private int x;
    private int y;
    private Color color;

    public Pyramid(int high, int width, int x,int y, Color color) {
    	this.high = high;
    	this.width = width;
    	this.x = x;
        this.y = y;
        this.color = color;
    }

    public int getHigh() {
        return high;
    }
    
    public int getWidth() {
        return width;
    }

    public int getX() {
        return x;
    }
    
    public int getY() {
        return y;
    }
    
    public Color getColor() {
        return color;
    }
    
    public void setHigh(int high) {
        this.high = high;
    }
    
    public void setWidth(int width) {
        this.width = width;
    }
    
    public void setX(int x) {
        this.x = x;
    }
    
    public void setY(int y) {
        this.y = y;
    }
    
    public void draw(Graphics g){
        g.setColor(getColor());
        g.drawRect(x,y,width, high);
    }
}
