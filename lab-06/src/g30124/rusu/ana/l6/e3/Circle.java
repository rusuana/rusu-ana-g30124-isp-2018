package g30124.rusu.ana.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

public class Circle implements Shape{
    
	private Color color;
	private int radius;
    private int x;
    private int y;
    private String id;
    private boolean fill;
    
    public Circle(Color color, int radius, int x,int y,String id,boolean fill) {
    	this.color=color;
        this.radius = radius;
    	this.x=x;
    	this.y=y;
    	this.id=id;
    	this.fill=fill;
    }

    public Color getColor() {
		return color;
	}
    
    public int getRadius() {
        return radius;
    }

	public String getId() {
		return id;
	}

	public boolean isFill() {
		return fill;
	}

    @Override
    public void draw(Graphics g) {
    	System.out.println("Drawing a circle with radius: "+radius);
        g.setColor(getColor());
        if(isFill()==true)
        	g.fillOval(x,y,radius,radius);
        else
        	g.drawOval(x,y,radius,radius);
    }
}
