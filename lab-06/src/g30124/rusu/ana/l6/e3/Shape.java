package g30124.rusu.ana.l6.e3;

import java.awt.Color;
import java.awt.Graphics;

public interface Shape {
    void draw(Graphics g);

	String getId();
	Color getColor();
}