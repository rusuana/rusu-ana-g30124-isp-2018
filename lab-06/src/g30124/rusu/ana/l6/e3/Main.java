package g30124.rusu.ana.l6.e3;

import java.awt.Color;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 80,150,200, "Circle_1",true);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 110,100,150,"Circle_2",false);
        b1.addShape(s2);
        Shape s3 = new Rectangle(Color.yellow, 150,100,200,300,"Rectangle",false);
        b1.addShape(s3);
        Shape s4 = new Rectangle(Color.blue, 80,50,150,290,"Rectangle_2",true);
        b1.addShape(s4);
      // b1.deleteById("Rectangle");
    }
}