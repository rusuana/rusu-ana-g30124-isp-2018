package g30124.rusu.ana.l6.e3;

import java.awt.*;

public class Rectangle implements Shape{

	private Color color;
	private int length;
    private int width;
    private int x;
    private int y;
    private String id;
    private boolean fill;

    public Rectangle(Color color, int length, int width, int x,int y, String id, boolean fill) {
        
        this.color=color;
        this.length = length;
        this.width = width;
    	this.x=x;
    	this.y=y;
    	this.id=id;
    	this.fill=fill;
    }
    public Color getColor() {
		return color;
	}

	public String getId() {
		return id;
	}

	public boolean isFill() {
		return fill;
	}
    @Override
    public void draw(Graphics g) {
    	System.out.println("Drawing a rectangle with width:"+width+" and length: " +length);
        g.setColor(getColor());
        if(isFill()==true)
    		g.fillRect(x,y,length, width);
        else
        	g.drawRect(x,y,length, width);
    }
    
} 
