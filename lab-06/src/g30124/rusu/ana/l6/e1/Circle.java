package g30124.rusu.ana.l6.e1;

import java.awt.*;

public class Circle extends Shape{

    private int radius;

    public Circle(Color color, int radius, int x,int y,String id,boolean fill) {
        super(color,x,y,id,fill);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    @Override
    public void draw(Graphics g) {
        System.out.println("Drawing a circle with radius: "+radius);
        g.setColor(getColor());
        g.drawOval(x,y,radius,radius);
        if(isFill()==true)
        	g.fillOval(x,y,radius,radius);	
    }
}
