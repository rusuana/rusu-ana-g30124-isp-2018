package g30124.rusu.ana.l6.e1;

import java.awt.*;

public abstract class Shape {

    protected Color color;
    protected int x;
    protected int y;
    protected String id;
    protected boolean fill;
    public Shape(Color color,int x,int y, String id,boolean fill) {
        this.color = color;
        this.x=x;
        this.y=y;
        this.id=id;
        this.fill=fill;
    }

    public Color getColor() {
        return color;
    }
    public String getId() {
        return id;
    }
    
    public boolean isFill() {
    	return fill;
    }
    public void setColor(Color color) {
        this.color = color;
    }

    public abstract void draw(Graphics g);
}