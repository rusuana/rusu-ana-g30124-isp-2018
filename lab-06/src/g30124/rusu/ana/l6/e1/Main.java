package g30124.rusu.ana.l6.e1;

import java.awt.*;

public class Main {
    public static void main(String[] args) {
        DrawingBoard b1 = new DrawingBoard();
        Shape s1 = new Circle(Color.RED, 90,150,200, "Circle_1",true);
        b1.addShape(s1);
        Shape s2 = new Circle(Color.GREEN, 100,100,150,"Circle_2",false);
        b1.addShape(s2);
        Shape s3 = new Rectangle(Color.yellow, 150,100,200,300,"Rectangle",false);
        b1.addShape(s3);
        b1.deleteById("Circle_1");
    }
}