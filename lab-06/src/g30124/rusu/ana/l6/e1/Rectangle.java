package g30124.rusu.ana.l6.e1;

import java.awt.*;

public class Rectangle extends Shape{

    private int length;
    private int width;

    public Rectangle(Color color, int length, int width, int x,int y, String id, boolean fill) {
        super(color,x,y,id,fill);
        this.length = length;
        this.width = width;
    }

    @Override
    public void draw(Graphics g) {
    	System.out.println("Drawing a rectangle with width:"+width+" and length: " +length);
        g.setColor(getColor());
        g.drawRect(x,y,length, width);
        if(isFill()==true)
    		g.fillRect(x,y,length, width);
    } 
} 
