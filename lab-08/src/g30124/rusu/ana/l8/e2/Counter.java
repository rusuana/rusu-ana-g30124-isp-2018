package g30124.rusu.ana.l8.e2;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

public class Counter {
	
    private static FileInputStream f;

	public static void main(String[] args) {
       
		int count = 0;
        File file = new File("E:\\Facultate\\sem 2\\ISP\\workspace\\lab-08\\src\\g30124\\rusu\\ana\\l8\\e2\\data.txt");
        
		System.out.println("Enter the letter you are looking for: ");
		Scanner g = new Scanner(System.in);
		char letter = g.next().charAt(0);
		g.close();
		
        if (file.exists())
            try {
                f = new FileInputStream(file);
                while (f.available() > 0){
                    char c = (char) f.read();
                    if (c == letter)
                        count++;
                }
                System.out.println("Exist "+count+" characters of "+letter+".");
            }catch (IOException ex){
                System.err.println(ex);
            }
        else
            System.out.println("File does not exist!");
    }
}
