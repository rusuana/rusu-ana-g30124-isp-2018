package g30124.rusu.ana.l8.e4;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Car implements Serializable{

	private String model;
	private int price;
	
	public Car(String model, int price) {
		this.model = model;
		this.price = price;
	}
	
	public String getModel() {
		return this.model;
	}
	
	public int getPrice() {
		return this.price;
	}
	
	public static void main(String[] args) throws Exception {
		
		Car c1 = new Car("BMW", 25000);
		Car c2 = new Car("Mercedes", 50000);
		Car c3 = new Car("VW", 25000);
		Car c4 = new Car("Ford", 10000);
		Car c5 = new Car("Ferrari", 230000);
		
        FileOutputStream fos = new FileOutputStream("E:\\Facultate\\sem 2\\ISP\\workspace\\lab-08\\src\\g30124\\rusu\\ana\\l8\\e4Garage.txt");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(c1);
        oos.writeObject(c2);
        oos.writeObject(c3);
        oos.writeObject(c4);
        oos.writeObject(c5);
        fos.close();
        
		FileInputStream fis = new FileInputStream("E:\\Facultate\\sem 2\\ISP\\workspace\\lab-08\\src\\g30124\\rusu\\ana\\l8\\e4Garage.txt");
		ObjectInputStream oin = new ObjectInputStream(fis);
		Car c6 = (Car)oin.readObject();
		Car c7 = (Car)oin.readObject();
		Car c8 = (Car)oin.readObject();
		Car c9 = (Car)oin.readObject();
		Car c10 = (Car)oin.readObject();
		fis.close();
		System.out.println(c6.getModel()+" "+c6.getPrice());
		System.out.println(c7.getModel()+" "+c7.getPrice());
		System.out.println(c8.getModel()+" "+c8.getPrice());
		System.out.println(c9.getModel()+" "+c9.getPrice());
		System.out.println(c10.getModel()+" "+c10.getPrice());	
	}
}
