package g30124.rusu.ana.l10.e6;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Chronometer extends JFrame {

    private static final long serialVersionUID = 3545053785228009472L;

    private JPanel panel;
    private JLabel tLabel;

    private JPanel buttonPanel;
    private JButton start;
    private JButton reset;
    private JButton stop;

    private byte centiseconds = 0;
    private byte seconds = 0;
    private short minutes = 0;

    private Runnable timeTask;
    private Runnable incrementTimeTask;
    private Runnable setTimeTask;
    private DecimalFormat timeFormatter;
    private boolean timerIsRunning = true;

    private ExecutorService executor = Executors.newCachedThreadPool();

    public Chronometer() {
        panel = new JPanel();
        panel.setLayout(new BorderLayout());

        tLabel = new JLabel();
        tLabel.setFont(new Font("Consolas", Font.PLAIN, 13));
        tLabel.setHorizontalAlignment(JLabel.CENTER);
        panel.add(tLabel);


        buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

        start = new JButton("Start");
        start.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if (!timerIsRunning)
                    timerIsRunning = true;
                executor.execute(timeTask);
            }
        });
        buttonPanel.add(start);

        reset = new JButton("Reset");
        reset.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                timerIsRunning = false;

                centiseconds = 0;
                seconds = 0;
                minutes = 0;

                tLabel.setText(timeFormatter.format(minutes) + ":" 
                        + timeFormatter.format(seconds) + "." 
                        + timeFormatter.format(centiseconds));
            }
        });

        buttonPanel.add(reset);
        stop = new JButton("Stop");
        stop.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                timerIsRunning = false;
            }
        });

        buttonPanel.add(stop);
        panel.add(buttonPanel, BorderLayout.SOUTH);
        
        timeFormatter = new DecimalFormat("00");

        timeTask = new Runnable(){
            public void run() {
                while(timerIsRunning) {
                    executor.execute(incrementTimeTask);
                    try{
                        Thread.sleep(10);
                    }
                    catch (InterruptedException ex){
                        ex.printStackTrace();
                    }
                 }
            }
        };

        incrementTimeTask = new Runnable(){
            public void run() {
                if (centiseconds < 99)
                    centiseconds++;
                else{  
                	if (centiseconds >= 99){
                        seconds++;
                        centiseconds = 00;
                    }
                    if (seconds == 60) {
                        minutes++;
                        seconds = 0;
                        centiseconds = 00;
                    }
                }
                executor.execute(setTimeTask);
            }
        };

        setTimeTask = new Runnable(){
            public void run(){
                tLabel.setText(timeFormatter.format(minutes) + ":" 
                        + timeFormatter.format(seconds) + "." 
                        + timeFormatter.format(centiseconds));
            }
        };

        tLabel.setText(timeFormatter.format(minutes) + ":" 
                + timeFormatter.format(seconds) + "." 
                + timeFormatter.format(centiseconds));

        add(panel);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLocationRelativeTo(null);
        setTitle("Chronometer.java");

        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        new Chronometer();
    }
}