package g30124.rusu.ana.l10.e3;


public class TwoCounters  extends Thread {
	Thread t;
	String name;
	TwoCounters(String name,Thread t){
        this.name=name;
        this.t=t;
	}
	public void run(){
       try {
    	   if (t!=null) {
               t.join();
		for(int i=101;i<201;i++){
              System.out.println(name+ " is in position i= "+i);
              try {
                    Thread.sleep((int)(Math.random() * 150));
              } catch(Exception e){e.printStackTrace();}
        }
        System.out.println(name + " is finished.");
    	   }
    	   else {
    		   for(int i=0;i<101;i++){
    	              System.out.println(name+ " is in position i= "+i);
    	              try {
    	                    Thread.sleep((int)(Math.random() * 150));
    	              } catch (Exception e){e.printStackTrace();}
    		   }	
    		   System.out.println(name + " is finished.");
    	   }
       }
       catch(Exception e){e.printStackTrace();}
	}
	public static void main(String[] args) {
		TwoCounters c1 = new TwoCounters("Trial 1",null);
		TwoCounters c2 = new TwoCounters("Trial 2",c1);

        c1.run();
        c2.run();
  }
}
