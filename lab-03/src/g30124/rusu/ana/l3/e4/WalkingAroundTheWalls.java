package g30124.rusu.ana.l3.e4;
import becker.robots.*;

public class WalkingAroundTheWalls {

    private static void createWalls(City Amsterdam){
        Wall blockAve = new Wall(Amsterdam, 1, 1, Direction.NORTH);
        Wall blockAve2 = new Wall(Amsterdam, 1, 2, Direction.NORTH);
        Wall blockAve3 = new Wall(Amsterdam, 1, 2, Direction.EAST);
        Wall blockAve4 = new Wall(Amsterdam, 2, 2, Direction.EAST);
        Wall blockAve5 = new Wall(Amsterdam, 2, 2, Direction.SOUTH);
        Wall blockAve6 = new Wall(Amsterdam, 1, 1, Direction.WEST);
        Wall blockAve7 = new Wall(Amsterdam, 2, 1, Direction.WEST);
        Wall blockAve8 = new Wall(Amsterdam, 2, 1, Direction.SOUTH);
    }

    public static void main(String[] args){
        City myCity = new City();
        createWalls(myCity);
        Robot pufi = new Robot(myCity, 0, 2, Direction.WEST);

        pufi.move();
        pufi.move();

        pufi.turnLeft();

        pufi.move();
        pufi.move();
        pufi.move();

        pufi.turnLeft();

        pufi.move();
        pufi.move();
        pufi.move();

        pufi.turnLeft();

        pufi.move();
        pufi.move();
        pufi.move();

        pufi.turnLeft();
        pufi.move();
    }
}
