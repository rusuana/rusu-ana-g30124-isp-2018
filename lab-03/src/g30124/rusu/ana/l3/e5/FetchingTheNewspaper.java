package g30124.rusu.ana.l3.e5;
import becker.robots.*;

public class FetchingTheNewspaper {

	    private static void createWalls(City aCity){
	        Wall blockAve = new Wall(aCity, 1, 1, Direction.NORTH);
	        Wall blockAve2 = new Wall(aCity, 1, 2, Direction.NORTH);
	        Wall blockAve3 = new Wall(aCity, 1, 2, Direction.EAST);
	        Wall blockAve5 = new Wall(aCity, 1, 2, Direction.SOUTH);
	        Wall blockAve6 = new Wall(aCity, 1, 1, Direction.WEST);
	        Wall blockAve7 = new Wall(aCity, 2, 1, Direction.WEST);
	        Wall blockAve8 = new Wall(aCity, 2, 1, Direction.SOUTH);
	    }

	    public static void main(String[] args){
	        City myCity = new City();
	        createWalls(myCity);
	        Robot pufi = new Robot(myCity, 1, 2, Direction.WEST);
	        Thing parcel = new Thing(myCity, 2, 2);

	        pufi.turnLeft();

	        pufi.turnLeft();
	        pufi.turnLeft();
	        pufi.turnLeft();


	        pufi.move();

	        pufi.turnLeft();

	        pufi.move();

	        pufi.turnLeft();

	        pufi.move();

	        pufi.pickThing();

	        pufi.turnLeft();
	        pufi.turnLeft();

	        pufi.move();

	        pufi.turnLeft();
	        pufi.turnLeft();
	        pufi.turnLeft();

	        pufi.move();

	        pufi.turnLeft();
	        pufi.turnLeft();
	        pufi.turnLeft();

	        pufi.move();

	        pufi.turnLeft();
	        pufi.turnLeft();
	        pufi.turnLeft();
	    }

	}
