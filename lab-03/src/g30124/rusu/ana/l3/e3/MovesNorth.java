package g30124.rusu.ana.l3.e3;
import becker.robots.*;

public class MovesNorth {
	
    public static void main(String[] args) {
        City myCity = new City();
        Robot pufi = new Robot(myCity, 1, 1, Direction.NORTH);
        
        //miscare spre nord de 5 ori
        for (int step = 0; step < 5; step++)
        	pufi.move();

        //rotirea lui pufi
        pufi.turnLeft();
        pufi.turnLeft();

        //miscare robot inapoi
        for (int step = 0; step < 5; step++)
        	pufi.move();
    }
}