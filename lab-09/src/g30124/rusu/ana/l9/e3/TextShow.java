package g30124.rusu.ana.l9.e3;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import javax.swing.*;

public class TextShow extends JFrame implements ActionListener{

	JButton Search;
	JTextField Text;
	JTextField Display;
	
	TextShow(){
		setTitle("Afiseaza fisier");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		init();
		setSize(300,300);
		setVisible(true);
	}
	
	public void init() {
		
		this.setLayout(null);
		
		Search = new JButton("Search");
		Search.setBounds(100, 40, 80, 20);
		Search.addActionListener(this);
		
		Text = new JTextField();
		Text.setBounds(10,10,265,20);
		
		Display = new JTextField();
		Display.setBounds(10, 70, 265, 185);
		Display.setEnabled(false);
		
		add(Text); add(Search); add(Display);
	}
	
	public void actionPerformed(ActionEvent c) {
		String str = new String();
		if(c.getSource() == Search)
			str = Text.getText();
			try {
				Display.setText(SearchFisier(str));
			} catch (IOException e) {
				e.printStackTrace();
			}
	}
	
	public String SearchFisier(String fis) throws IOException {
		
		BufferedReader in = new BufferedReader(new FileReader(fis));
		String line;
		while((line = in.readLine()) != null) {
			return line;
		}
		in.close();
		return null;
	}
	
	public static void main(String[] args) {
		new TextShow();
	}

}