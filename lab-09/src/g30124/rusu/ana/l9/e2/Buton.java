package g30124.rusu.ana.l9.e2;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class Buton implements ActionListener{

    JButton Button1 = new JButton("Increment");
    JFrame o = new JFrame();  
    JTextField Display = new JTextField(15);
    JButton Button2 = new JButton("Reset");
    JButton Button3 = new JButton("Add 10");

    int i = 0;

    public Buton(){

    	Display.setText("Number :"+i);
    	Display.setEnabled(false);
    	 
        o.setTitle("Numarator");
        o.setVisible(true);
        o.setSize(250,150);
        o.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        o.setResizable(true);
        
        o.setLayout(new FlowLayout());
        
        o.add(Display);
        o.add(Button1);
        o.add(Button2);
        o.add(Button3);
        
        Button1.addActionListener(this);
        Button2.addActionListener(this);
        Button3.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e){
    	
        if(e.getSource() == Button1){
            i++;
            Display.setText("Number: "+i);
        }
        
        if(e.getSource() == Button2){
        	i=0;
        	Display.setText("Number: "+i);
        }
        
        if(e.getSource() == Button3){
        	i+=10;
        	Display.setText("Number: "+i);
        }
    }
    
    public static void main(String args[]){
        new Buton();
    }
}