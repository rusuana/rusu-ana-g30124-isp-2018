package g30124.rusu.ana.l4.e4;

import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

public class TestAuthor {
	
	@Test
	public void TestName() {
		Author a = new Author("Dan Puric","Dan.Puric@yahoo.com",'m');
		assertTrue(a.getName() == "Dan Puric");
	}
	
	@Test
    public void testName() {
		Author a = new Author("Dan Puric","Dan.Puric@yahoo.com",'m');
        assertEquals("Dan Puric",a.getName());
    }
	
	@Test
	public void TestEmail() {
		Author a = new Author("Dan Puric","Dan.Puric@yahoo.com",'m');
		a.setEmail("Dan_Puric@gmail.com");
		assertTrue(a.getEmail() == "Dan_Puric@gmail.com");
	}
	
	@Test
	public void testEmail() {
		Author a = new Author("Dan Puric","Dan.Puric@yahoo.com",'m');
		 assertEquals("Dan.Puric@yahoo.com",a.getEmail());
	}
	
	@Test
	public void TestGende() {
		Author a = new Author("Dan Puric","Dan.Puric@yahoo.com",'m');
		assertTrue(a.getGender() == 'm');
	}
	
	@Test
	public void testGender() {
		Author a = new Author("Dan Puric","Dan.Puric@yahoo.com",'m');
		 assertEquals('m',a.getGender());
	}
}
