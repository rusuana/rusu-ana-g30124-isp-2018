package g30124.rusu.ana.l4.e4;

public class Author {
	
	private String name;
	private String email;
	private char gender;
	
	public Author(String name, String email, char gender) {
		
		this.name = name;
		this.email = email;
		this.gender = gender;
	}
	
	public Author() {
		name ="Dan Puric";
		email ="Dan_Puric@gmail.com";
		gender = 'm';
	}
	
	public String getName() {
		return name;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		 this.email = email;
	}
	
	public char getGender() {
		return gender;
	}
	
	public String toString() {
		return new String("Autor-"+this.name+" ("+this.gender+") at "+this.email);
	}
	
	
	public static void main(String[] args) {
		
		Author a = new Author("Dan Puric","Dan.Puric@yahoo.com",'m');
		System.out.println("Name of author is: " +a.getName());
		System.out.println("Gender of author is: " +a.getGender());
		System.out.println("Email of author is: " +a.getEmail()+'\n');
		System.out.print(a.toString());
	}

}
