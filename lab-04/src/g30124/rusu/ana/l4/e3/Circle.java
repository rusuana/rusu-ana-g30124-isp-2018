package g30124.rusu.ana.l4.e3;

public class Circle {

	protected double radius;
	private String color;
	
	public Circle(){
		this.radius = 1.0;
		this.color = "red";
	}
	
	public Circle(double radius) {
		this.radius = radius;
	}
	
	public double getRadius() {
		return this.radius;
	}
	
	public double getArea() {
		return Math.PI*this.radius*this.radius;
	}
	
	public static void main(String[] args) {
		Circle c = new Circle(4.5);
		System.out.println("Radius of circle is: " +c.getRadius());
		System.out.print("Area of circle is: " +c.getArea());
	}
}
