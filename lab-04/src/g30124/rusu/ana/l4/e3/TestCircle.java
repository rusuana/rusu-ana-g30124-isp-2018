package g30124.rusu.ana.l4.e3;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class TestCircle {
	
	@Test
	public void TestSet() {
		
		Circle c = new Circle(7.5);
		assertTrue(c.getRadius() == 7.5);
	}
	
	@Test
	public void TestArea() {
		Circle c = new Circle(4.5);
		assertTrue(c.getArea() == 63.61725123519331);
	}
	
}