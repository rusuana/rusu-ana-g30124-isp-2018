package g30124.rusu.ana.l4.e7;

import org.junit.Test;
import static org.junit.Assert.assertTrue;

public class TestCylinder {

	@Test
	public void TestRadius() {
		Cylinder c = new Cylinder(3.1,12.47);
		assertTrue(c.getRadius() == 3.1);
	}
	
	@Test
	public void TestHeight() {
		Cylinder c = new Cylinder(3.4,11.7);
		assertTrue(c.getHeight() == 11.7);
	}
	
	@Test
	public void TestVolume() {
		Cylinder c = new Cylinder(2.0,3.0);
		assertTrue(c.getVolume() == 188.49555921538757);
	}	
}