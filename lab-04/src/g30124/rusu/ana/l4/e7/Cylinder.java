package g30124.rusu.ana.l4.e7;

import g30124.rusu.ana.l4.e3.*;


public class Cylinder extends Circle{

	private double height = 1.0;
	
	public Cylinder(){
		this.height = 6;
		this.radius = 2;
	}
	
	public Cylinder(double radius) {
		super(radius);
	}
	
	public Cylinder(double radius,double height){
		super(radius);
		this.height = height;
	}
	
	public double getHeight(){
		return this.height;
	}
	
	 @Override
	    public double getArea() {
	        return (2*Math.PI*getRadius()*height + 2*Math.PI*getRadius()*getRadius());
	    }
	 
	public double getVolume(){
		return getArea()*height;
	}
	
	public static void main(String[] args){
		
		Cylinder c = new Cylinder(2.0,3.0);
		System.out.println("Height of cylinder is: " +c.getHeight());
		System.out.println("Volume of cylinder is: " +c.getVolume());
	}
}