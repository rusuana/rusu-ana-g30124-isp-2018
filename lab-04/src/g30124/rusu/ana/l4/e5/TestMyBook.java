package g30124.rusu.ana.l4.e5;

import g30124.rusu.ana.l4.e4.*;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class TestMyBook {
	
	@Test
	public void testPrice() {
		Book b = new Book("Pe aripile vantului",null, 45.6);
		assertTrue(b.getPrice() == 45.6);
	}
	
	@Test
	public void testBookName() {
		Book b = new Book("Pe aripile vantului",null, 45.6);
		assertEquals(b.getName(), "Pe aripile vantului");}
	
	@Test
	public void testGetQtyInStock() {
		Book b = new Book("Pe aripile vantului",null, 45.6,87);
		assertTrue(b.getQtyInStock() == 87);
	}
	
	@Test
	public void TestAuthor() {
		Author author = new Author("Margaret Mitchell","Margaret.Mitchell@yahoo.com",'f');
		Book b = new Book("Pe aripile vantului",author, 45.6);
		assertEquals(b.getAuthor(),author.getName());
	}
}
