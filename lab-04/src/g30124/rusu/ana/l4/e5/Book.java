package g30124.rusu.ana.l4.e5;
import g30124.rusu.ana.l4.e4.*;

public class Book extends Author{

	private String name;
	private Author author;
	private double price;
	private int qtyInStock;
	
	public Book(String name,Author author, double price) {
		
		this.name = name;
		this.author = author;
		this.price = price;	
	}
	
	public Book(String name,Author author,double price,int qtyInStock) {

		this.name = name;
		this.price = price;
		this.author = author;
		this.qtyInStock = qtyInStock;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getAuthor() {
		return author.getName();
	}
	
	public double getPrice() {
		return this.price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public int getQtyInStock() {
		return this.qtyInStock;
	}
	
	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock = qtyInStock;
	}
	
	public String toString() {
		return new String("'"+getName()+"' by "+author.getName()+" ("+author.getGender()+") at "+author.getEmail());
	}
	
	public static void main(String[] args) {
		
		Author author = new Author("Dan Puric", "Dan.Puric@yahoo.com", 'm');
		Book b = new Book("Despre omul frumos",author,24.7);
		b.setQtyInStock(184);
		System.out.println("Name of book is: " +b.getName());
		System.out.println("Name of author is: " +b.getAuthor());
		System.out.println("Number of books in stock is: " +b.getQtyInStock());
		System.out.println("Price of a book is: " +b.getPrice()+'\n');
		System.out.println(b.toString());
	}
}
