package g30124.rusu.ana.l4.e6;

import g30124.rusu.ana.l4.e4.*;

public class Book extends Author{
	
	private String name;
	private double price;
	private int qtyInStock;
	private Author[] author;
	
	public Book(String name,Author[] author, double price) {
		
		this.name = name;
		this.author = author;
		this.price = price;
	}
	
	public Book(String name,Author[] author,double price,int qtyInStock) {

		this.name = name;
		this.price = price;
		this.author = author;
		this.qtyInStock = qtyInStock;
	}
	
	public String getName() {
		return this.name;
	}
	
	public Author[] getAuthor() {
		return this.author;
	}
	
	public void printAuthors(Author[] author) {
		
		for(int i=0;i<author.length;i++)
			System.out.println(author[i].getName());
	}
	
	public double getPrice() {
		return this.price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public int getQtyInStock() {
		return this.qtyInStock;
	}
	
	public void setQtyInStock(int qtyInStock) {
		this.qtyInStock = qtyInStock;
	}
	
	public String toString() {
		return new String(getName()+" by "+author.length+" authors");
	}

	public static void main(String[] args) {
		
		Author[] author = new Author[4];
		author[0] = new Author("Dan Puric", "Dan.Puric@yahoo.com", 'm');
		author[1] = new Author("Margaret Mitchell", "Margaret_Mitchell@yahoo.com", 'f');
		author[2] = new Author("Danielle Steel", "DanielleSteel@gmail.com", 'f');
		author[3] = new Author("Alice Munro", "Munro@yahoo.com", 'f');
		Book b = new Book("Accidental Heroes",author,39.4,261);
		System.out.println("Price of a book is: " +b.getPrice());
		System.out.println("Name of book is:" +b.getName());
		System.out.println("Quantity in stock is: " +b.getQtyInStock()+'\n');
		b.printAuthors(author);
		System.out.println('\n'+b.toString());
	}
}
