package g30124.rusu.ana.l4.e6;

import g30124.rusu.ana.l4.e4.*;
import g30124.rusu.ana.l4.e5.Book;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;


public class TestBook {

	@Test
	public void TestName() {
		Author author = new Author("Margaret Mitchell","Margaret.Mitchell@yahoo.com",'f');
		Book b = new Book("Pe aripile vantului",author, 45.6);
		assertEquals(b.getName(),"Pe aripile vantului");
	}
	
	@Test
	public void TestQuantity() {
		Author author = new Author("Margaret Mitchell","Margaret.Mitchell@yahoo.com",'f');
		Book b = new Book("Pe aripile vantului",author, 45.6,37);
		assertTrue(b.getQtyInStock() == 37);
	}
	

	@Test
	public void TestPrice() {
		Author author = new Author("Margaret Mitchell","Margaret.Mitchell@yahoo.com",'f');
		Book b = new Book("Pe aripile vantului",author, 45.6);
		assertTrue(b.getPrice() == 45.6);
	}
}
