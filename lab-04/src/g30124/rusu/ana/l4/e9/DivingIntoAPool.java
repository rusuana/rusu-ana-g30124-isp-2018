package g30124.rusu.ana.l4.e9;

import becker.robots.*;

public class DivingIntoAPool extends Robot{

    public DivingIntoAPool(City c, int i, int j, Direction d){
        super(c, i, j, d);
    }

    public void Drive(){
        this.move();
        this.turnLeft();
        this.turnLeft();
        this.turnLeft();
        this.move();
        this.turnLeft();
        this.turnLeft();
        this.turnLeft();
        this.move();
        this.turnLeft();
        this.turnLeft();
        this.turnLeft();
        this.turnLeft();
        this.move();
        this.move();
        this.move();
        this.turnLeft();
        this.turnLeft();
    }

    public static void main(String[] args){

        City NY = new City();
        Wall blockAve0 = new Wall(NY, 2, 2, Direction.EAST);
        Wall blockAve1 = new Wall(NY, 3, 2, Direction.EAST);
        Wall blockAve2 = new Wall(NY, 4, 2, Direction.EAST);
        Wall blockAve3 = new Wall(NY, 4, 3, Direction.WEST);
        Wall blockAve4 = new Wall(NY, 4, 4, Direction.SOUTH);
        Wall blockAve5 = new Wall(NY, 4, 3, Direction.SOUTH);
        Wall blockAve6 = new Wall(NY, 4, 4, Direction.EAST);
        Wall blockAve7 = new Wall(NY, 2, 3, Direction.NORTH);

        DivingIntoAPool karel = new DivingIntoAPool(NY, 1, 3, Direction.NORTH);
        karel.Drive();
    }
}