package g30124.rusu.ana.l4.e8;


public class Square extends Rectangle{

	public Square(){
		this.width = 2.0;
		this.length = 3.0;
	}
	
	public Square(double side){
		this.width = side;
	}
	public Square(double width, double length){	
		super(width,length);
	}
	
	@Override
	public void setWidth(double width){
		super.width = width;
	}
	
	@Override
	public void setLength(double length){
		super.length = super.getWidth();
	}
	
	@Override
	public String toString(){
		return new String("A Square with side="+super.getLength()+", which is a subclass of "+super.toString());
	}
	
	public static void main(String[] args){
		Square s = new Square(3.0,7.0);
		s.setWidth(3.5);
		s.setLength(15.0);
		System.out.println("Width of square is: " +s.getWidth());
		System.out.println("Lenght of square is: " +s.getLength());
		System.out.println(s.toString());
	}
}

