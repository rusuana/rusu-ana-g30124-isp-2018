package g30124.rusu.ana.l4.e8;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestShape {

	@Test
	public void TestColor() {
		Shape s = new Shape("Red",false);
		s.setColor("Green");
		assertEquals(s.getColor(),"Green");
	}
	
	@Test
	public void TestValue() {
		Shape s = new Shape("Blue",false);
		assertEquals(s.isFilled(),false);
	}
	
	@Test
	public void TestPerimeterCircle() {
		Circle c = new Circle(5.2);
		assertTrue(c.getPerimeter() == 32.67256359733385);
	}
	
	@Test
	public void TestRadiusCircle() {
		Circle c = new Circle(7.4);
		assertTrue(c.getRadius() == 7.4);
	}
	
	@Test
	public void TestAreaCircle() {
		Circle c = new Circle(5.3);
		assertTrue(c.getArea() == 88.24733763933727);
	}
	
	@Test
	public void TestRectangelWidth() {
		Rectangle r = new Rectangle(6.1,3.4);
		assertTrue(r.getWidth() == 6.1);
	}
	
	@Test
	public void TestRectangleLength() {
		Rectangle r = new Rectangle(6.1,3.4);
		assertTrue(r.getLength() == 3.4);
	}
	
	@Test
	public void TestRectanglePerimeter() {
		Rectangle r = new Rectangle(6.0,3.0);
		assertTrue(r.getPerimeter() == 18.0);
	}
	
	@Test
	public void TestRectangleArea() {
		Rectangle r = new Rectangle(6.0,3.0);
		assertTrue(r.getArea() == 18.0);
	}
	
	@Test
	public void TestSquareWidth() {
		Square s = new Square();
		s.setWidth(6.0);
		s.setLength(3.0);
		assertTrue(s.getWidth() == s.getLength());
	}
}