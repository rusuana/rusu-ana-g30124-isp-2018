package g30124.rusu.ana.l4.e8;


public class Shape{

	protected String color;
	protected boolean filled;
	
	public Shape(){
		this.color = "green";
		this.filled = false;
	}
	
	public Shape(String color, boolean filled){
		this.color = color;
		this.filled = filled;
	}
	
	public boolean isFilled(){
		return this.filled;
	}
	
	public void setFilled(boolean filled){
		this.filled = filled;
	}
	
	public String getColor(){
		return this.color;
	}
	
	public void setColor(String color){
		this.color = color;
	}
	
	public String toString(){
		String x;
		if(isFilled() == true)
			x = "filled";
		else
			x = "not filled";
		return new String("A Shape with color of "+getColor()+" and "+x);
	}
	
	public static void main(String[] args){
		Shape s = new Shape("pink",false);
		System.out.println("Color of shape is: " +s.getColor());
		System.out.println("Filled of shape is: " +s.isFilled());
		System.out.println(s.toString());
	}
}
