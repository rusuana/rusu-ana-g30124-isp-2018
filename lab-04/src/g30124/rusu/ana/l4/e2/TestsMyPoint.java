package g30124.rusu.ana.l4.e2;
import org.junit.Test;
import org.junit.Before;
import static junit.framework.Assert.assertEquals;

public class TestsMyPoint {
    private MyPoint p;

    @Before
    public void create(){
        p = new MyPoint();
    }
    
    @Test
    public void shouldsetX(){
    p.setX(5);
    assertEquals(5,p.getX());
    }

    @Test
    public void shouldsetY(){
        p.setY(3);
        assertEquals(3,p.getY());
    }

    @Test
    public void shouldsetXY(){
        p.setXY(8,4);
        assertEquals(8,p.getX());
        assertEquals(4,p.getY());
    }
    
    @Test

    public void Distance() {
        MyPoint p1 = new MyPoint(3,8);
        assertEquals(3.16, p1.distance(2,5),0.01);
    }
    
    
   
}

